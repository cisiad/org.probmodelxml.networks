<?xml version="1.0" encoding="UTF-8"?>
<ProbModelXML formatVersion="0.2.0">
  <ProbNet type="InfluenceDiagram">
    <Comment showWhenOpeningNetwork="true"><![CDATA[<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      In this ID, when <i>Do test?</i> = <i>no </i>the probabilities for the 
      outcomes are the same for <i>Disease</i> = <i>present</i> as for <i>Disease</i> 
      = <i>absent</i>. This way the test does not contribute any information 
      and its diagnostic value is null. Those probabilities (in this example 
      we have arbitrarily chosen 0.75 and 0.25) are irrelevant: the expected 
      utilities and the optimal policies are the same as if we had added a 
      dummy state for <i>Result of test</i>. Even the probabilities of <i>Result 
      of test</i> are the same for the optimal strategy, in which the test is 
      done. Only if we impose the policy <i>Do test?</i> = <i>no</i> can we 
      see any effect of those probabilities: as expected, the probabilities of <i>Result 
      of test</i> for this sub-optimal strategy are just the probabilities we 
      have arbitrarily chosen. One drawback of this ID is that its equivalent 
      decision tree contains branches with non-null probability that should 
      not be there.
    </p>
  </body>
</html>]]></Comment>
    <DecisionCriteria>
      <Criterion name="---" unit="---" />
    </DecisionCriteria>
    <AdditionalProperties>
      <Property name="Version" value="1.0" />
      <Property name="VisualPrecision" value="0.0" />
      <Property name="KindOfGraph" value="directed" />
    </AdditionalProperties>
    <Variables>
      <Variable name="Disease" type="finiteStates" role="chance">
        <Coordinates x="137" y="65" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="X" />
        </AdditionalProperties>
        <States>
          <State name="absent" />
          <State name="present" />
        </States>
      </Variable>
      <Variable name="Result of test" type="finiteStates" role="chance">
        <Coordinates x="354" y="159" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="Y" />
        </AdditionalProperties>
        <States>
          <State name="negative" />
          <State name="positive" />
        </States>
      </Variable>
      <Variable name="Therapy" type="finiteStates" role="decision">
        <Coordinates x="432" y="282" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="D" />
        </AdditionalProperties>
        <States>
          <State name="no" />
          <State name="yes" />
        </States>
      </Variable>
      <Variable name="Do test?" type="finiteStates" role="decision">
        <Coordinates x="590" y="67" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="T" />
        </AdditionalProperties>
        <States>
          <State name="no" />
          <State name="yes" />
        </States>
      </Variable>
      <Variable name="Health state" type="numeric" role="utility">
        <Coordinates x="228" y="412" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Min" value="0.0" />
          <Property name="Precision" value="2.0" />
          <Property name="Max" value="1.0" />
          <Property name="Title" value="U" />
        </AdditionalProperties>
        <Unit />
        <Precision>2.0</Precision>
        <Criterion name="---" />
      </Variable>
      <Variable name="Cost of test" type="numeric" role="utility">
        <Coordinates x="684" y="194" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Min" value="0.0" />
          <Property name="Precision" value="2.0" />
          <Property name="Max" value="1.0" />
          <Property name="Title" value="C" />
        </AdditionalProperties>
        <Unit />
        <Precision>2.0</Precision>
        <Criterion name="---" />
      </Variable>
      <Variable name="Cost of therapy" type="numeric" role="utility">
        <Coordinates x="526" y="413" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="---" />
      </Variable>
    </Variables>
    <Links>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="Health state" />
      </Link>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="Result of test" />
      </Link>
      <Link directed="true">
        <Variable name="Result of test" />
        <Variable name="Therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="Health state" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="Cost of therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Do test?" />
        <Variable name="Cost of test" />
      </Link>
      <Link directed="true">
        <Variable name="Do test?" />
        <Variable name="Therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Do test?" />
        <Variable name="Result of test" />
      </Link>
    </Links>
    <Potentials>
      <Potential type="Table" role="conditionalProbability">
        <Variables>
          <Variable name="Disease" />
        </Variables>
        <Values>0.86 0.14</Values>
      </Potential>
      <Potential type="Table" role="conditionalProbability">
        <Variables>
          <Variable name="Result of test" />
          <Variable name="Disease" />
          <Variable name="Do test?" />
        </Variables>
        <Values>0.25 0.75 0.25 0.75 0.97 0.03 0.09 0.91</Values>
      </Potential>
      <Potential type="Table" role="utility">
        <UtilityVariable name="Health state" />
        <Variables>
          <Variable name="Disease" />
          <Variable name="Therapy" />
        </Variables>
        <Values>10.0 3.0 9.0 8.0</Values>
      </Potential>
      <Potential type="Table" role="utility">
        <UtilityVariable name="Cost of test" />
        <Variables>
          <Variable name="Do test?" />
        </Variables>
        <Values>0.0 -0.2</Values>
      </Potential>
      <Potential type="Table" role="utility">
        <UtilityVariable name="Cost of therapy" />
        <Variables>
          <Variable name="Therapy" />
        </Variables>
        <Values>0.0 -0.75</Values>
      </Potential>
    </Potentials>
  </ProbNet>
  <InferenceOptions>
    <MulticriteriaOptions>
      <SelectedAnalysisType>UNICRITERION</SelectedAnalysisType>
      <Unicriterion>
        <Scales>
          <Scale Criterion="---" Value="1.0" />
        </Scales>
      </Unicriterion>
      <CostEffectiveness>
        <Scales>
          <Scale Criterion="---" Value="1.0" />
        </Scales>
        <CE_Criteria>
          <CE_Criterion Criterion="---" Value="Cost" />
        </CE_Criteria>
      </CostEffectiveness>
    </MulticriteriaOptions>
  </InferenceOptions>
</ProbModelXML>
