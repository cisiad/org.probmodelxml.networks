<?xml version="1.0" encoding="UTF-8"?>
<ProbModelXML formatVersion="1.0.0">
  <ProbNet type="InfluenceDiagram">
    <DecisionCriteria>
      <Criterion name="direct costs" unit="€" />
      <Criterion name="private costs" unit="€" />
      <Criterion name="effectiveness" unit="QALY" />
    </DecisionCriteria>
    <AdditionalProperties>
      <Property name="Version" value="1.0" />
      <Property name="VisualPrecision" value="0.0" />
      <Property name="KindOfGraph" value="directed" />
    </AdditionalProperties>
    <Variables>
      <Variable name="Disease" type="finiteStates" role="chance">
        <Coordinates x="148" y="78" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="X" />
        </AdditionalProperties>
        <States>
          <State name="absent" />
          <State name="present" />
        </States>
      </Variable>
      <Variable name="Test" type="finiteStates" role="chance">
        <Coordinates x="317" y="200" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="Y" />
        </AdditionalProperties>
        <States>
          <State name="not-performed" />
          <State name="negative" />
          <State name="positive" />
        </States>
      </Variable>
      <Variable name="Therapy" type="finiteStates" role="decision">
        <Coordinates x="373" y="327" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="D" />
        </AdditionalProperties>
        <States>
          <State name="no" />
          <State name="therapy 1" />
          <State name="therapy 2" />
        </States>
      </Variable>
      <Variable name="Dec:Test" type="finiteStates" role="decision">
        <Coordinates x="528" y="82" />
        <AdditionalProperties>
          <Property name="Relevance" value="7.0" />
          <Property name="Title" value="T" />
        </AdditionalProperties>
        <States>
          <State name="no" />
          <State name="yes" />
        </States>
      </Variable>
      <Variable name="Cost of therapy (system)" type="numeric" role="utility">
        <Coordinates x="642" y="437" />
        <Unit />
        <Precision>1.0</Precision>
        <Criterion name="direct costs" />
      </Variable>
      <Variable name="Cost of test" type="numeric" role="utility">
        <Coordinates x="655" y="206" />
        <Unit />
        <Precision>1.0</Precision>
        <Criterion name="direct costs" />
      </Variable>
      <Variable name="Effectiveness" type="numeric" role="utility">
        <Coordinates x="186" y="468" />
        <Unit />
        <Precision>0.1</Precision>
        <Criterion name="effectiveness" />
      </Variable>
      <Variable name="Cost of therapy (patient)" type="numeric" role="utility">
        <Coordinates x="497" y="534" />
        <Unit />
        <Precision>1.0</Precision>
        <Criterion name="private costs" />
      </Variable>
    </Variables>
    <Links>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="Test" />
      </Link>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="Effectiveness" />
      </Link>
      <Link directed="true">
        <Variable name="Test" />
        <Variable name="Therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="Cost of therapy (system)" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="Effectiveness" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="Cost of therapy (patient)" />
      </Link>
      <Link directed="true">
        <Variable name="Dec:Test" />
        <Variable name="Therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Dec:Test" />
        <Variable name="Test" />
      </Link>
      <Link directed="true">
        <Variable name="Dec:Test" />
        <Variable name="Cost of test" />
      </Link>
    </Links>
    <Potentials>
      <Potential type="ProbTable">
        <Variables>
          <Variable name="Disease" />
        </Variables>
        <Values>0.86 0.14</Values>
        <UncertainValues>
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Beta" name="prevalence">14.0 86.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="ProbTable">
        <Variables>
          <Variable name="Test" />
          <Variable name="Dec:Test" />
          <Variable name="Disease" />
        </Variables>
        <Values>1.0 0.0 0.0 0.0 0.93 0.06999999999999995 1.0 0.0 0.0 0.0 0.09999999999999998 0.9</Values>
        <UncertainValues>
          <Value />
          <Value />
          <Value />
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Beta" name="specificity">93.0 7.0</Value>
          <Value distribution="Complement" name="">1.0</Value>
          <Value />
          <Value />
          <Value />
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Beta" name="sensitivity">90.0 10.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Cost of therapy (system)" />
          <Variable name="Therapy" />
        </Variables>
        <Values>0.0 20000.0 70000.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv" name="cost of therapy 1 (system)">20000.0 4000.0</Value>
          <Value distribution="Gamma-mv" name="cost of therapy 2 (system)">70000.0 14000.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Cost of test" />
          <Variable name="Dec:Test" />
        </Variables>
        <Values>0.0 150.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv" name="cost of test">150.0 30.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Effectiveness" />
          <Variable name="Disease" />
          <Variable name="Therapy" />
        </Variables>
        <Values>9.0E9 1.0800000000000007E9 8.91E9 3.6E9 8.370000000000007E9 5.850000000000001E9</Values>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Cost of therapy (patient)" />
          <Variable name="Therapy" />
        </Variables>
        <Values>0.0 350.0 100.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv" name="cost of therapy 1 (system)">350.0 70.0</Value>
          <Value distribution="Gamma-mv" name="cost of therapy 2 (system)">100.0 20.0</Value>
        </UncertainValues>
      </Potential>
    </Potentials>
  </ProbNet>
  <InferenceOptions>
    <MulticriteriaOptions>
      <SelectedAnalysisType>UNICRITERION</SelectedAnalysisType>
      <Unicriterion>
        <Unit>€</Unit>
        <Scales>
          <Scale Criterion="direct costs" Value="-1.0" />
          <Scale Criterion="private costs" Value="0.0" />
          <Scale Criterion="effectiveness" Value="30000.0" />
        </Scales>
      </Unicriterion>
      <CostEffectiveness>
        <Scales>
          <Scale Criterion="direct costs" Value="1.0" />
          <Scale Criterion="private costs" Value="0.0" />
          <Scale Criterion="effectiveness" Value="1.0" />
        </Scales>
        <CE_Criteria>
          <CE_Criterion Criterion="direct costs" Value="Cost" />
          <CE_Criterion Criterion="private costs" Value="Cost" />
          <CE_Criterion Criterion="effectiveness" Value="Effectiveness" />
        </CE_Criteria>
      </CostEffectiveness>
    </MulticriteriaOptions>
  </InferenceOptions>
</ProbModelXML>
