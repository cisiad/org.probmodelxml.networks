<?xml version="1.0" encoding="UTF-8"?>
<ProbModelXML formatVersion="1.0.0">
  <ProbNet type="InfluenceDiagram">
    <Comment showWhenOpeningNetwork="false"><![CDATA[<<Double click to add/modify comment>>]]></Comment>
    <DecisionCriteria>
      <Criterion name="Cost" unit="£" />
      <Criterion name="Effectiveness" unit="QALY" />
    </DecisionCriteria>
    <Variables>
      <Variable name="Disease" type="finiteStates" role="chance">
        <Coordinates x="246" y="53" />
        <States>
          <State name="absent" />
          <State name="present" />
        </States>
      </Variable>
      <Variable name="New test" type="finiteStates" role="chance">
        <Coordinates x="428" y="406" />
        <States>
          <State name="not done" />
          <State name="negative" />
          <State name="positive" />
        </States>
      </Variable>
      <Variable name="Dec: New test" type="finiteStates" role="decision">
        <Coordinates x="657" y="315" />
        <States>
          <State name="no" />
          <State name="yes" />
        </States>
      </Variable>
      <Variable name="C: Test" type="numeric" role="utility">
        <Coordinates x="888" y="409" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="Cost" />
      </Variable>
      <Variable name="Therapy" type="finiteStates" role="decision">
        <Coordinates x="431" y="561" />
        <States>
          <State name="no" />
          <State name="therapy 1" />
          <State name="therapy 2" />
        </States>
      </Variable>
      <Variable name="QALE" type="numeric" role="utility">
        <Comment><![CDATA[<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      Quality-adjusted life expectancy.
    </p>
  </body>
</html>]]></Comment>
        <Coordinates x="179" y="689" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="Effectiveness" />
      </Variable>
      <Variable name="C: Therapy" type="numeric" role="utility">
        <Coordinates x="742" y="696" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="Cost" />
      </Variable>
      <Variable name="Test" type="finiteStates" role="chance">
        <Coordinates x="457" y="191" />
        <States>
          <State name="not done" />
          <State name="negative" />
          <State name="positive" />
        </States>
      </Variable>
      <Variable name="Dec: Test" type="finiteStates" role="decision">
        <Coordinates x="664" y="65" />
        <States>
          <State name="no" />
          <State name="yes" />
        </States>
      </Variable>
      <Variable name="C: Cheap test" type="numeric" role="utility">
        <Coordinates x="881" y="164" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="Cost" />
      </Variable>
    </Variables>
    <Links>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="New test" />
      </Link>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="QALE" />
      </Link>
      <Link directed="true">
        <Variable name="Disease" />
        <Variable name="Test" />
      </Link>
      <Link directed="true">
        <Variable name="New test" />
        <Variable name="Therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Dec: New test" />
        <Variable name="New test" />
      </Link>
      <Link directed="true">
        <Variable name="Dec: New test" />
        <Variable name="C: Test" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="QALE" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy" />
        <Variable name="C: Therapy" />
      </Link>
      <Link directed="true">
        <Variable name="Test" />
        <Variable name="Dec: New test" />
      </Link>
      <Link directed="true">
        <Variable name="Dec: Test" />
        <Variable name="Test" />
      </Link>
      <Link directed="true">
        <Variable name="Dec: Test" />
        <Variable name="C: Cheap test" />
      </Link>
    </Links>
    <Potentials>
      <Potential type="ProbTable">
        <Variables>
          <Variable name="Disease" />
        </Variables>
        <Values>0.86 0.14</Values>
        <UncertainValues>
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Beta" name="prevalence">14.0 86.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="ProbTable">
        <Variables>
          <Variable name="New test" />
          <Variable name="Dec: New test" />
          <Variable name="Disease" />
        </Variables>
        <Values>1.0 0.0 0.0 0.0 0.93 0.07 1.0 0.0 0.0 0.0 0.09999999999999998 0.9</Values>
        <UncertainValues>
          <Value />
          <Value />
          <Value />
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Beta" name="specificity">93.0 7.0</Value>
          <Value distribution="Complement">1.0</Value>
          <Value />
          <Value />
          <Value />
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Beta" name="sensitivity">90.0 10.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="C: Test" />
          <Variable name="Dec: New test" />
        </Variables>
        <Values>0.0 150.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv" name="cost of test">150.0 30.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="QALE" />
          <Variable name="Disease" />
          <Variable name="Therapy" />
        </Variables>
        <Values>10.0 1.2 9.9 4.0 9.3 6.5</Values>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="C: Therapy" />
          <Variable name="Therapy" />
        </Variables>
        <Values>0.0 20000.0 70000.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv">20000.0 4000.0</Value>
          <Value distribution="Gamma-mv">70000.0 14000.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="ProbTable">
        <Variables>
          <Variable name="Test" />
          <Variable name="Dec: Test" />
          <Variable name="Disease" />
        </Variables>
        <Values>1.0 0.0 0.0 0.0 0.91 0.08999999999999997 1.0 0.0 0.0 0.0 0.3 0.7</Values>
        <UncertainValues>
          <Value />
          <Value />
          <Value />
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Beta" name="specificity (cheap)">91.0 9.0</Value>
          <Value distribution="Complement">1.0</Value>
          <Value />
          <Value />
          <Value />
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Beta" name="sensitivity (cheap)">70.0 30.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="C: Cheap test" />
          <Variable name="Dec: Test" />
        </Variables>
        <Values>0.0 18.0</Values>
      </Potential>
    </Potentials>
  </ProbNet>
  <InferenceOptions>
    <MulticriteriaOptions>
      <SelectedAnalysisType>UNICRITERION</SelectedAnalysisType>
      <Unicriterion>
        <Unit>£</Unit>
        <Scales>
          <Scale Criterion="Cost" Value="-1.0" />
          <Scale Criterion="Effectiveness" Value="30000.0" />
        </Scales>
      </Unicriterion>
      <CostEffectiveness>
        <Scales>
          <Scale Criterion="Cost" Value="1.0" />
          <Scale Criterion="Effectiveness" Value="1.0" />
        </Scales>
        <CE_Criteria>
          <CE_Criterion Criterion="Cost" Value="Cost" />
          <CE_Criterion Criterion="Effectiveness" Value="Effectiveness" />
        </CE_Criteria>
      </CostEffectiveness>
    </MulticriteriaOptions>
  </InferenceOptions>
</ProbModelXML>
