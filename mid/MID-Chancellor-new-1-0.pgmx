<?xml version="1.0" encoding="UTF-8"?>
<ProbModelXML formatVersion="1.0.0">
  <ProbNet type="MID">
    <Comment showWhenOpeningNetwork="true"><![CDATA[<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      MID for analyzing the cost-effectiveness of two therapies for HIV:
    </p>
    <p style="margin-top: 0">
      <i>- monotherapy</i>, which means &quot;only zidovudine&quot;.
    </p>
    <p style="margin-top: 0">
      <i>- combined therapy</i>, which means &quot;zidovudine plus lamivudine for 
      the first three years (i.e., three cycles), then zidovudine alone&quot;.
    </p>
    <p style="margin-top: 0">
      
    </p>
    <p style="margin-top: 0">
      Original paper:
    </p>
    <p style="margin-top: 0">
      Chancellor JV, et al.
    </p>
    <p style="margin-top: 0">
      Modelling the cost effectiveness of lamivudine/zidovudine combination 
      therapy in HIV infection.
    </p>
    <p style="margin-top: 0">
      <i>Pharmacoeconomics </i>1997;12:54-66.
    </p>
    <p style="margin-top: 0">
      
    </p>
    <p style="margin-top: 0">
      There is another MID that faithfully replicates the Excel version of 
      this model built by A. Briggs--see <a href="http://www.probmodelxml.org/networks/mid/MID-Chancellor.pgmx">www.probmodelxml.org/networks/mid/MID-Chancellor.pgmx</a>.
    </p>
    <p style="margin-top: 0">
      
    </p>
    <p style="margin-top: 0">
      That MID was built by I&#241;igo Bermejo and F. Javier D&#237;ez in July 2013. 
      This new MID is the result of the revision done by F. J. D&#237;ez in June 
      2016.
    </p>
    <p style="margin-top: 0">
      
    </p>
    <p style="margin-top: 0">
      Open the properties dialog of each node to obtain additional 
      information. See also the chapter on MIDs in <a href="http://www.openmarkov.org/docs/tutorial">OpenMarkov's 
      tutorial</a>.
    </p>
  </body>
</html>
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->
<!--StartFragment-->]]></Comment>
    <DecisionCriteria>
      <Criterion name="cost" unit="£" />
      <Criterion name="effectiveness" unit="QALY" />
    </DecisionCriteria>
    <TimeUnit unit="YEAR" Value="1.0" />
    <Variables>
      <Variable name="State" timeSlice="0" type="finiteStates" role="chance">
        <Comment><![CDATA[<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      Health state of the VIH patient: State A (200&lt;cd4 cells/mm3&lt;500); State 
      B (cd4&lt;200); State C (AIDs); Death
    </p>
  </body>
</html>]]></Comment>
        <Coordinates x="439" y="121" />
        <States>
          <State name="Dead" />
          <State name="State C" />
          <State name="State B" />
          <State name="State A" />
        </States>
      </Variable>
      <Variable name="State" timeSlice="1" type="finiteStates" role="chance">
        <Coordinates x="732" y="121" />
        <States>
          <State name="Dead" />
          <State name="State C" />
          <State name="State B" />
          <State name="State A" />
        </States>
      </Variable>
      <Variable name="Time in treatment" timeSlice="0" type="numeric" role="chance">
        <Coordinates x="222" y="59" />
        <Unit />
        <Precision>1.0</Precision>
        <Thresholds>
          <Threshold value="0.0" belongsTo="right" />
          <Threshold value="Infinity" belongsTo="right" />
        </Thresholds>
      </Variable>
      <Variable name="Time in treatment" timeSlice="1" type="numeric" role="chance">
        <Coordinates x="585" y="59" />
        <Unit />
        <Precision>1.0</Precision>
        <Thresholds>
          <Threshold value="0.0" belongsTo="right" />
          <Threshold value="Infinity" belongsTo="right" />
        </Thresholds>
      </Variable>
      <Variable name="Therapy choice" type="finiteStates" role="decision">
        <Coordinates x="107" y="127" />
        <States>
          <State name="monotherapy" />
          <State name="combination therapy" />
        </States>
      </Variable>
      <Variable name="Cost AZT" timeSlice="0" type="numeric" role="utility">
        <Coordinates x="156" y="387" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="cost" />
      </Variable>
      <Variable name="Life years" timeSlice="0" type="numeric" role="utility">
        <Coordinates x="313" y="164" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="effectiveness" />
      </Variable>
      <Variable name="Direct medical cost" timeSlice="0" type="numeric" role="utility">
        <Coordinates x="557" y="177" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="cost" />
      </Variable>
      <Variable name="Community care cost" timeSlice="0" type="numeric" role="utility">
        <Coordinates x="463" y="211" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="cost" />
      </Variable>
      <Variable name="Therapy applied" timeSlice="0" type="finiteStates" role="chance">
        <Coordinates x="227" y="311" />
        <States>
          <State name="none" />
          <State name="monotherapy" />
          <State name="combination therapy" />
        </States>
      </Variable>
      <Variable name="Transition inhibited" timeSlice="1" type="finiteStates" role="chance">
        <Comment><![CDATA[<html>
  <head>
    
  </head>
  <body>
    <p style="margin-top: 0">
      In Chancellor's model, the effect of combined therapy is represented by 
      a reduction of the relative risk of progressing to more severe degrees 
      of the disease, as explained in section &quot;Clinical efficay&quot; of their 
      paper. In this model, this effect is modelled by assuming that combined 
      therapy sometimes inhibit the transitions, with a probability equal to 
      the relative risk.
    </p>
  </body>
</html>]]></Comment>
        <Coordinates x="562" y="311" />
        <States>
          <State name="no" />
          <State name="yes" />
        </States>
      </Variable>
      <Variable name="Cost lamivudine" timeSlice="0" type="numeric" role="utility">
        <Coordinates x="307" y="386" />
        <Unit />
        <Precision>0.01</Precision>
        <Criterion name="cost" />
      </Variable>
    </Variables>
    <Links>
      <Link directed="true">
        <Variable name="State" timeSlice="0" />
        <Variable name="State" timeSlice="1" />
      </Link>
      <Link directed="true">
        <Variable name="State" timeSlice="0" />
        <Variable name="Life years" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="State" timeSlice="0" />
        <Variable name="Direct medical cost" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="State" timeSlice="0" />
        <Variable name="Community care cost" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="State" timeSlice="0" />
        <Variable name="Therapy applied" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="Time in treatment" timeSlice="0" />
        <Variable name="Time in treatment" timeSlice="1" />
      </Link>
      <Link directed="true">
        <Variable name="Time in treatment" timeSlice="0" />
        <Variable name="Therapy applied" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy choice" />
        <Variable name="Therapy applied" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy applied" timeSlice="0" />
        <Variable name="Transition inhibited" timeSlice="1" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy applied" timeSlice="0" />
        <Variable name="Cost lamivudine" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="Therapy applied" timeSlice="0" />
        <Variable name="Cost AZT" timeSlice="0" />
      </Link>
      <Link directed="true">
        <Variable name="Transition inhibited" timeSlice="1" />
        <Variable name="State" timeSlice="1" />
      </Link>
    </Links>
    <Potentials>
      <Potential type="Delta">
        <Variables>
          <Variable name="State" timeSlice="0" />
        </Variables>
        <State>State A</State>
      </Potential>
      <Potential type="ProbTable">
        <Variables>
          <Variable name="State" timeSlice="1" />
          <Variable name="Transition inhibited" timeSlice="1" />
          <Variable name="State" timeSlice="0" />
        </Variables>
        <Values>1.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.2498570612 0.7501429388 0.0 0.0 0.0 1.0 0.0 0.0 0.0119236884 0.4069952305 0.5810810811 0.0 0.0 0.0 1.0 0.0 0.0098039216 0.0668973472 0.2018454441 0.7214532872 0.0 0.0 0.0 1.0</Values>
        <UncertainValues>
          <Value />
          <Value />
          <Value />
          <Value />
          <Value />
          <Value />
          <Value />
          <Value />
          <Value distribution="Beta">437.0 1312.0</Value>
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Exact">0.0</Value>
          <Value distribution="Exact">0.0</Value>
          <Value />
          <Value />
          <Value />
          <Value />
          <Value distribution="Dirichlet">15.0</Value>
          <Value distribution="Dirichlet">512.0</Value>
          <Value distribution="Dirichlet">731.0</Value>
          <Value distribution="Exact">0.0</Value>
          <Value />
          <Value />
          <Value />
          <Value />
          <Value distribution="Dirichlet">17.0</Value>
          <Value distribution="Dirichlet">116.0</Value>
          <Value distribution="Dirichlet">350.0</Value>
          <Value distribution="Dirichlet">1251.0</Value>
          <Value />
          <Value />
          <Value />
          <Value />
        </UncertainValues>
      </Potential>
      <Potential type="Delta">
        <Variables>
          <Variable name="Time in treatment" timeSlice="0" />
        </Variables>
        <NumericValue>0.0</NumericValue>
      </Potential>
      <Potential type="CycleLengthShift">
        <Variables>
          <Variable name="Time in treatment" timeSlice="1" />
          <Variable name="Time in treatment" timeSlice="0" />
        </Variables>
      </Potential>
      <Potential type="Tree/ADD">
        <Variables>
          <Variable name="Cost AZT" timeSlice="0" />
          <Variable name="Therapy applied" timeSlice="0" />
        </Variables>
        <TopVariable name="Therapy applied" timeSlice="0" />
        <Branches>
          <Branch>
            <States>
              <State name="none" />
            </States>
            <Potential type="UnivariateDistr" distribution="Exact">
              <Variables>
                <Variable name="Cost AZT" timeSlice="0" />
              </Variables>
              <Values>0.0</Values>
            </Potential>
          </Branch>
          <Branch>
            <States>
              <State name="monotherapy" />
              <State name="combination therapy" />
            </States>
            <Potential type="UnivariateDistr" distribution="Exact">
              <Variables>
                <Variable name="Cost AZT" timeSlice="0" />
              </Variables>
              <Values>2278.0</Values>
            </Potential>
          </Branch>
        </Branches>
      </Potential>
      <Potential type="Tree/ADD">
        <Variables>
          <Variable name="Life years" timeSlice="0" />
          <Variable name="State" timeSlice="0" />
        </Variables>
        <TopVariable name="State" timeSlice="0" />
        <Branches>
          <Branch>
            <States>
              <State name="State C" />
              <State name="State B" />
              <State name="State A" />
            </States>
            <Potential type="UnivariateDistr" distribution="Exact">
              <Variables>
                <Variable name="Life years" timeSlice="0" />
              </Variables>
              <Values>1.0</Values>
            </Potential>
          </Branch>
          <Branch>
            <States>
              <State name="Dead" />
            </States>
            <Potential type="UnivariateDistr" distribution="Exact">
              <Variables>
                <Variable name="Life years" timeSlice="0" />
              </Variables>
              <Values>0.0</Values>
            </Potential>
          </Branch>
        </Branches>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Direct medical cost" timeSlice="0" />
          <Variable name="State" timeSlice="0" />
        </Variables>
        <Values>0.0 6948.0 1774.0 1701.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv">6948.0 6948.0</Value>
          <Value distribution="Gamma-mv">1774.0 1774.0</Value>
          <Value distribution="Gamma-mv">1701.0 1701.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Community care cost" timeSlice="0" />
          <Variable name="State" timeSlice="0" />
        </Variables>
        <Values>0.0 2059.0 1278.0 1055.0</Values>
        <UncertainValues>
          <Value />
          <Value distribution="Gamma-mv">2059.0 2059.0</Value>
          <Value distribution="Gamma-mv">1278.0 1278.0</Value>
          <Value distribution="Gamma-mv">1055.0 1055.0</Value>
        </UncertainValues>
      </Potential>
      <Potential type="Tree/ADD">
        <Variables>
          <Variable name="Therapy applied" timeSlice="0" />
          <Variable name="Time in treatment" timeSlice="0" />
          <Variable name="Therapy choice" />
          <Variable name="State" timeSlice="0" />
        </Variables>
        <TopVariable name="State" timeSlice="0" />
        <Branches>
          <Branch>
            <States>
              <State name="Dead" />
            </States>
            <Potential type="Delta">
              <Variables>
                <Variable name="Therapy applied" timeSlice="0" />
              </Variables>
              <State>none</State>
            </Potential>
          </Branch>
          <Branch>
            <States>
              <State name="State C" />
              <State name="State B" />
              <State name="State A" />
            </States>
            <Potential type="Tree/ADD">
              <Variables>
                <Variable name="Therapy applied" timeSlice="0" />
                <Variable name="Time in treatment" timeSlice="0" />
                <Variable name="Therapy choice" />
                <Variable name="State" timeSlice="0" />
              </Variables>
              <TopVariable name="Therapy choice" />
              <Branches>
                <Branch>
                  <States>
                    <State name="monotherapy" />
                  </States>
                  <Potential type="Delta">
                    <Variables>
                      <Variable name="Therapy applied" timeSlice="0" />
                    </Variables>
                    <State>monotherapy</State>
                  </Potential>
                </Branch>
                <Branch>
                  <States>
                    <State name="combination therapy" />
                  </States>
                  <Potential type="Tree/ADD">
                    <Variables>
                      <Variable name="Therapy applied" timeSlice="0" />
                      <Variable name="Time in treatment" timeSlice="0" />
                      <Variable name="Therapy choice" />
                      <Variable name="State" timeSlice="0" />
                    </Variables>
                    <TopVariable name="Time in treatment" timeSlice="0" />
                    <Branches>
                      <Branch>
                        <Thresholds>
                          <Threshold value="0.0" belongsTo="right" />
                          <Threshold value="3.0" belongsTo="right" />
                        </Thresholds>
                        <Potential type="Delta">
                          <Variables>
                            <Variable name="Therapy applied" timeSlice="0" />
                          </Variables>
                          <State>combination therapy</State>
                        </Potential>
                      </Branch>
                      <Branch>
                        <Thresholds>
                          <Threshold value="3.0" belongsTo="right" />
                          <Threshold value="Infinity" belongsTo="right" />
                        </Thresholds>
                        <Potential type="Delta">
                          <Variables>
                            <Variable name="Therapy applied" timeSlice="0" />
                          </Variables>
                          <State>monotherapy</State>
                        </Potential>
                      </Branch>
                    </Branches>
                  </Potential>
                </Branch>
              </Branches>
            </Potential>
          </Branch>
        </Branches>
      </Potential>
      <Potential type="ProbTable">
        <Comment><![CDATA[<html>
  <head>
    
  </head>
  <body>
    The probability of inhibiting the transition is 0.509, the same as the 
    relative risk of combined therapy with respect to monotherapy. This value, 
    as well, as the 95% confidence interval 0.365-0.710, appear in Table IV of 
    the paper by Chancellor et al. They are taken from a meta-analysis: 
    Staszewski et al. Reductions in HIV-1 disease progression for 
    zidovudine/lamivudine relative to control treatments: a meta-analysis of 
    controlled trials. AIDS, 1997, 11:477-83. The difference of this MID with 
    the Excel version of the same model is that the second-order distribution 
    for this parameter is not modelled with a log-normal distribution, which 
    might lead to transition probabilities below 0 or above 1, but with a Beta.
  </body>
</html>]]></Comment>
        <Variables>
          <Variable name="Transition inhibited" timeSlice="1" />
          <Variable name="Therapy applied" timeSlice="0" />
        </Variables>
        <Values>1.0 0.0 1.0 0.0 0.4909999056 0.5090000944</Values>
        <UncertainValues>
          <Value />
          <Value />
          <Value />
          <Value />
          <Value distribution="Complement">1.0</Value>
          <Value distribution="Beta">17.62564 17.00233</Value>
        </UncertainValues>
      </Potential>
      <Potential type="UnivariateDistr" distribution="Exact">
        <Variables>
          <Variable name="Cost lamivudine" timeSlice="0" />
          <Variable name="Therapy applied" timeSlice="0" />
        </Variables>
        <Values>0.0 0.0 2085.50445</Values>
      </Potential>
    </Potentials>
  </ProbNet>
  <InferenceOptions>
    <MulticriteriaOptions>
      <SelectedAnalysisType>COST_EFFECTIVENESS</SelectedAnalysisType>
      <Unicriterion>
        <Scales>
          <Scale Criterion="cost" Value="1.0" />
          <Scale Criterion="effectiveness" Value="1.0" />
        </Scales>
      </Unicriterion>
      <CostEffectiveness>
        <Scales>
          <Scale Criterion="cost" Value="1.0" />
          <Scale Criterion="effectiveness" Value="1.0" />
        </Scales>
        <CE_Criteria>
          <CE_Criterion Criterion="cost" Value="Cost" />
          <CE_Criterion Criterion="effectiveness" Value="Effectiveness" />
        </CE_Criteria>
      </CostEffectiveness>
    </MulticriteriaOptions>
    <TemporalOptions>
      <Slices>1</Slices>
      <Transition>END</Transition>
      <DiscountRates>
        <DiscountRate Criterion="cost" value="0.035" unit="YEAR" />
        <DiscountRate Criterion="effectiveness" value="0.035" unit="YEAR" />
      </DiscountRates>
    </TemporalOptions>
  </InferenceOptions>
</ProbModelXML>
